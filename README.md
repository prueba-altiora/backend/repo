# Repo

_Proyecto para el repositorio de las ordenes realizadas por un cliente_

_La URL para poder ver las Apis documentadas al momento de correr el proyecto es:_
```sh
   http://localhost:9003/swagger-ui.html#
   ```

## Comenzando 🚀

_Estas instrucciones permitirán obtener una copia del proyecto en funcionamiento en su máquina local para propósitos poder realizar la revisión._
1. Clonar el repositorio
```sh
   git clone https://gitlab.com/prueba-altiora/backend/repo.git
   ```

### Pre-requisitos 📋

_Que cosas que se necesitan para correr el software_

1. Clonar el proyecto de admin el cual gestiona los clientes y los artículos para realizar la orden.
```
git clone https://gitlab.com/prueba-altiora/backend/admin.git
```

2. Crear una base de datos PostgreSql con el nombre de orden.
```
Create database orden;
```

3. En el archivo application-dev.yaml, cambiar las credenciales (username,password) para que pueda conectar a su base de datos.
```
spring:
  datasource:
    url: jdbc:postgresql://localhost/orden
    username: postgres
    password: root
  jpa:
    database: postgresql
    hibernate:
      ddl-auto: update
```
4. Tener disponible el puerto 9003, ya que la aplicación correra en ese puerto, o en el archivo application.yaml cambiar el puerto en el que desea que corra la aplicación
```
server:
  port: 9003
```
5. Tener instalado Spring Tool Suit o Visual Studio Code para poder correr la aplicación.
6. Tener instalado java jdk8. 
7. Descargar las dependencias maven que se encuentran en el archio pom.xml del proyecto.
```
Esto se realiza haciendo un UpdateProject.
```
8.- Antes de correr el proyecto debe tener corriendo el proyecto de admin, ya que existe una comunicación de microservicios para ambos proyectos.

## Autor ✒️


* **Jefferson Esteban Yaguana Montero** 


