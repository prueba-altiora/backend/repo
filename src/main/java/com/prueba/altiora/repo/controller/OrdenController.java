package com.prueba.altiora.repo.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.prueba.altiora.repo.exception.NotStoreException;
import com.prueba.altiora.repo.model.Orden;
import com.prueba.altiora.repo.payload.OrdenPayload;
import com.prueba.altiora.repo.service.imp.OrdenServiceImp;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value = "/orden")
@Api(tags = "Administracion de Ordenes")
public class OrdenController {
	
	@Autowired
	private OrdenServiceImp ordenService;
	
	@PostMapping ("/crear")
	@ApiOperation(value = "Registro de una orden")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Orden registrado"),
			@ApiResponse(code = 404, message = "No se encontro el cliente o el producto seleccionado."),
			@ApiResponse(code = 400, message = "No se pudo realizar el registro de la orden."),})
	public Orden crearCliente(@Valid @RequestBody OrdenPayload ordenPayload) throws NotStoreException{
		return ordenService.crearOrden(ordenPayload);
	}
	
	@GetMapping("/listar")
	@ApiOperation(value = "Obtener todas las ardenes registradas", notes = "<b>Ejemplo de envío</b></br>URL -> localhost:9003/orden/listar")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Listado de las ordenes registradas") })
	public List<Orden> listarOrdenes(){
		return ordenService.listarOrdenes();
	}

}
