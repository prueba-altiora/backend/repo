package com.prueba.altiora.repo.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.prueba.altiora.repo.payload.admin.Articulo;
import com.prueba.altiora.repo.payload.admin.Cliente;

@FeignClient(url = "${cliente.admin}", name = "admin")
public interface AdminServiceClient {
	
	@GetMapping("/cliente/obtener/{identificacion}")
	ResponseEntity<Cliente> obtenerClientePorId(@PathVariable(value = "identificacion") String identificacion);
	
	@GetMapping("/articulo/obtener/{id}")
	ResponseEntity<Articulo> obtenerArticuloPorId(@PathVariable(value = "id") Long id);
	
	@PutMapping("articulo/actualizar")
	ResponseEntity<Articulo> actualizarArticulo(@RequestBody Articulo articulo);

}
