package com.prueba.altiora.repo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.prueba.altiora.repo.model.Orden;

@Repository
public interface OrdenRepository extends JpaRepository<Orden, Long>{

}
