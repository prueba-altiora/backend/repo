package com.prueba.altiora.repo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.prueba.altiora.repo.model.Detalle;

@Repository
public interface DetalleRepository extends JpaRepository<Detalle, Long> {

}
