package com.prueba.altiora.repo.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "orden")
public class Orden {

	@ApiModelProperty(value = "Id de la orden, este id es Autoincremental.", dataType = "Long")
	@Id
	@GeneratedValue(strategy =GenerationType.IDENTITY)
	private Long idOrden;
	
	@NotNull (message = "La fecha no puede ser vacía")
	@ApiModelProperty(value = "Fecha en que se realiza la orden",dataType = "Date")
	@Column(name = "fecha",  nullable = false)
	private Date fecha;
	
	@NotEmpty(message = "El nombre del cliente es obligatorio")
	@NotNull(message = "El nombre del cliente no debe ser null")
	@ApiModelProperty(value = "Nombre del cliente que realizó la orden",dataType = "String")
	@Column(name = "nombreCliente", length = 200)
	private String nombreCliente;
	
	@ApiModelProperty(value = "Lista de detalles de los artículos")
	@OneToMany(mappedBy = "orden",fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<Detalle> detalles;
}
