package com.prueba.altiora.repo.model;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "detalle")
public class Detalle {
	
	@ApiModelProperty(value = "Id del detalle, este id es Autoincremental.", dataType = "Long")
	@Id
	@GeneratedValue(strategy =GenerationType.IDENTITY)
	private Long idDetalle;
	
	@NotEmpty(message = "El nombre del artículo es obligatorio")
	@NotNull(message = "El nombre del artículo no debe ser null")
	@ApiModelProperty(value = "Nombre del artículo que se seleccionó para la orden",dataType = "String")
	@Column(name = "nombreArticulo", length = 200)
	private String nombreArticulo;
	
	@NotNull(message = "La cantidad del artículo no debe ser null")
	@Min(value = 1,message = "La cantidad de artículos debe ser mínimo 1")
	@ApiModelProperty(value = "Cantidad de artículos que se seleccionó para la orden",dataType = "Long")
	@Column(name = "cantidad", length = 200)
	private Long cantidad;
	
	@NotNull(message = "El precio del artículo no puede ser null")
	@Digits(integer = 14, fraction = 6,message = "El precio unitario del artículo puede tener 14 dígitos para el entero y 6 dígitos para los decimales")
	@ApiModelProperty(value = "Precio unitario del artículo a registrarse.", required = true, dataType = "BigDecimal")
	@Column(name = "precioUnitario", nullable = false)
	private BigDecimal precioUnitario;
	
	@ApiModelProperty(value = "Total del artículo a registrarse.", required = false, dataType = "BigDecimal")
	@Column(name = "total", nullable = false)
	private BigDecimal total;
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "idOrden", nullable = false)
	private Orden orden;
	

}
