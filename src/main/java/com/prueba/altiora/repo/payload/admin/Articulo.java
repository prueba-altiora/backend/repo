package com.prueba.altiora.repo.payload.admin;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class Articulo {
	
	private Long idArticulo;
	private String codigo;
	private String nombre;
	private Long stock;
	private BigDecimal precioUnitario;
}
