package com.prueba.altiora.repo.payload;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class OrdenPayload {

	@NotNull (message = "La fecha no puede ser vacía")
	@ApiModelProperty(value = "Fecha en que se realiza la orden",required = true,dataType = "Date")
	private Date fecha;	
	
	@NotNull(message = "El nombre del cliente no debe ser null")
	@ApiModelProperty(value = "Id del cliente que realizó la orden",required = true,dataType = "String")
	private String idCliente;
	
	@NotNull(message = "Los detalles no pueden ser nulos")
	@NotEmpty(message = "Los detalles son obligatorios")
	@ApiModelProperty(value = "Detalles para la orden",required = true,dataType = "DetallePayload")
	List<DetallePayload> detalles;
}
