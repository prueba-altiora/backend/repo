package com.prueba.altiora.repo.payload.admin;

import lombok.Data;

@Data
public class Cliente {

	private String identificacion;
	private String nombres;
	private String apellidos;
	
}
