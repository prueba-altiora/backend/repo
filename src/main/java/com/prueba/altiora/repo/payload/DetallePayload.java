package com.prueba.altiora.repo.payload;

import javax.persistence.Column;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class DetallePayload {

	@NotNull(message = "El id del artículo no debe ser null")
	@ApiModelProperty(value = "Id del artículo que se seleccionó para la orden",dataType = "Long")
	private Long idArticulo;
	
	@NotNull(message = "La cantidad del artículo no debe ser null")
	@Min(value = 1,message = "La cantidad de artículos debe ser mínimo 1")
	@ApiModelProperty(value = "Cantidad de artículos que se seleccionó para la orden",dataType = "Long")
	@Column(name = "cantidad")
	private Long cantidad;
}
