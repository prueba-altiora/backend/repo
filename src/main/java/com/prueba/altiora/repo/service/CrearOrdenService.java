package com.prueba.altiora.repo.service;

import com.prueba.altiora.repo.exception.NotStoreException;
import com.prueba.altiora.repo.model.Orden;
import com.prueba.altiora.repo.payload.OrdenPayload;

public interface CrearOrdenService {

	Orden crearOrden(OrdenPayload ordenPayload) throws NotStoreException;
}
