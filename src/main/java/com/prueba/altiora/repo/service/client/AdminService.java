package com.prueba.altiora.repo.service.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.prueba.altiora.repo.client.AdminServiceClient;
import com.prueba.altiora.repo.exception.EntityNotFoundException;
import com.prueba.altiora.repo.exception.NotStoreException;
import com.prueba.altiora.repo.payload.admin.Articulo;
import com.prueba.altiora.repo.payload.admin.Cliente;

@Service
public class AdminService {

	@Autowired
	AdminServiceClient adminClient;

	public Cliente obtenerClientePorId(String identificacion) {

		ResponseEntity<Cliente> response = adminClient.obtenerClientePorId(identificacion);

		if (response.getStatusCodeValue() != 200) {
			throw new EntityNotFoundException("El cliente seleccionado no existe", Cliente.class, "Identificación",
					identificacion);
		}
		return response.getBody();
	}

	public Articulo obtenerArticuloPorId(Long id) {

		ResponseEntity<Articulo> response = adminClient.obtenerArticuloPorId(id);

		if (response.getStatusCodeValue() != 200) {
			throw new EntityNotFoundException("El articulo seleccionado no existe", Articulo.class, "Id ",
					id.toString());
		}
		return response.getBody();
	}
	
	public Articulo actualizarArticulo (Articulo articulo) throws NotStoreException {
		ResponseEntity<Articulo> response = adminClient.actualizarArticulo(articulo);
		if (response.getStatusCodeValue() != 200) {
			throw new NotStoreException("No se pudo actualizar el stock en el articulo","No se pudo actualizar el stock en el articulo");
		}
		return response.getBody();
	}

}
