package com.prueba.altiora.repo.service;

import java.util.List;

import com.prueba.altiora.repo.model.Orden;

public interface ListarOrdenService {

	List<Orden> listarOrdenes();
}
