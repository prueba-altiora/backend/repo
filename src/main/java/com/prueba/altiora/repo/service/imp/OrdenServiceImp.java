package com.prueba.altiora.repo.service.imp;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prueba.altiora.repo.exception.NotStoreException;
import com.prueba.altiora.repo.model.Detalle;
import com.prueba.altiora.repo.model.Orden;
import com.prueba.altiora.repo.payload.DetallePayload;
import com.prueba.altiora.repo.payload.OrdenPayload;
import com.prueba.altiora.repo.payload.admin.Articulo;
import com.prueba.altiora.repo.payload.admin.Cliente;
import com.prueba.altiora.repo.repository.OrdenRepository;
import com.prueba.altiora.repo.service.CrearOrdenService;
import com.prueba.altiora.repo.service.ListarOrdenService;
import com.prueba.altiora.repo.service.client.AdminService;

@Service
public class OrdenServiceImp implements CrearOrdenService, ListarOrdenService {

	@Autowired
	AdminService adminService;

	@Autowired
	OrdenRepository ordenRepoository;

	@Transactional
	@Override
	public Orden crearOrden(OrdenPayload ordenPayload) throws NotStoreException {

		Cliente cliente = adminService.obtenerClientePorId(ordenPayload.getIdCliente());

		List<Detalle> detalles = new ArrayList<>();

		final Orden orden = Orden.builder()
				.nombreCliente(cliente.getNombres().concat(" ").concat(cliente.getApellidos()))
				.fecha(ordenPayload.getFecha()).build();

		for (DetallePayload o : ordenPayload.getDetalles()) {
			Articulo articulo = adminService.obtenerArticuloPorId(o.getIdArticulo());

			if (o.getCantidad() > articulo.getStock()) {
				throw new NotStoreException("No existe Stock para el artículo ".concat(articulo.getNombre()), "No existe Stock para el artículo ".concat(articulo.getNombre()));
			}

			Detalle detalle = Detalle.builder().cantidad(o.getCantidad()).nombreArticulo(articulo.getNombre())
					.precioUnitario(articulo.getPrecioUnitario())
					.total(articulo.getPrecioUnitario().multiply(new BigDecimal(o.getCantidad()))).orden(orden).build();
			detalles.add(detalle);
			
			articulo.setStock(articulo.getStock()-o.getCantidad());		
			adminService.actualizarArticulo(articulo);			
		}
		Orden ordenAlmacenada = orden;
		ordenAlmacenada.setDetalles(detalles);
		ordenAlmacenada = ordenRepoository.save(ordenAlmacenada);

		if (ordenAlmacenada == null) {
			throw new NotStoreException("No se pudo guardar la orden.", "La orden no se pudo realizar.");
		}

		return ordenAlmacenada;
	}

	@Override
	public List<Orden> listarOrdenes() {
		return ordenRepoository.findAll();
	}

}
